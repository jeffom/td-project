﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour
{
    public delegate void ClickEventHandler(IClickable obj);        
    public static event ClickEventHandler OnClickEvent;

    [SerializeField]
    Camera m_pInputCamera;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //did we clicked anything
            var selectedObj = CheckCollision();
            
            if (OnClickEvent != null)
            {
                OnClickEvent(selectedObj);
            }
        }
    }


    IClickable CheckCollision()
    {
        Camera pCam = GetCam();

        Vector3 mouseScreenPos = Input.mousePosition;
        Ray ray = pCam.ScreenPointToRay(mouseScreenPos);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000f))
        {
            IClickable hitObj = (IClickable)hit.collider.transform.GetComponentInParent(typeof(IClickable));
            if (hitObj != null)
            {
                return hitObj;
            }
        }

        return null;
    }

    Camera GetCam()
    {
        Camera pCam = m_pInputCamera != null ? m_pInputCamera : Camera.main;

        return pCam;
    }

    //pseudo cursor for debugging
    void OnDrawGizmos()
    {
        //if (Input.GetMouseButtonDown(0))
        {
            Vector3 mouseScreenPos = Input.mousePosition;
            Camera pCam = m_pInputCamera != null ? m_pInputCamera : Camera.main;
            Vector3 curMousPos = pCam.ScreenToWorldPoint(mouseScreenPos);
            curMousPos.y = 0;
            Gizmos.color = Color.blue;
            //Gizmos.DrawSphere(curMousPos - new Vector3(0, 30f), 1f);

            Ray ray = pCam.ScreenPointToRay(mouseScreenPos);

            Debug.DrawRay(ray.origin, ray.direction * 30, Color.yellow);
        }
    }
}
