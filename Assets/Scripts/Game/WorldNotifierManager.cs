﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Global bus for exchangin messages and listen events
/// </summary>
public static class WorldNotifierManager  {

	public delegate void GameNotification(string evtName, params object[] p);
	public static event GameNotification notificationListener;

	public static void TriggerEvent(string evtName, params object[] p)
	{
		//try
		{
			if(notificationListener != null )
			{
				notificationListener(evtName, p);
			}
		}
		//catch{
		//	Debug.LogError("Error while triggering event");
		//}
	}
}
