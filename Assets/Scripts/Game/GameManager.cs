﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public int m_lives = 20;

	public bool m_isGameRunning;

	[SerializeField]
	PlayerInfo m_UIplayerInfo;

	// Use this for initialization
	void Start () {
		m_isGameRunning = true;
		WorldNotifierManager.notificationListener += HandleWorldEvents;

		m_UIplayerInfo.SetLivesLeft(m_lives);
	}
	
	// Update is called once per frame
	void Update () {
		if( m_lives == 0 && m_isGameRunning)
		{
			GameOver();
		}
	}

	void GameOver()
	{
		m_isGameRunning = false;

		Debug.Log("Game Over");

        UnityEngine.SceneManagement.SceneManager.LoadScene("gameover");
	}

	public void HandleWorldEvents(string evt, params object[] p)
	{
		if( evt == WorldEventsType.EnemyReachedGoal.ToString() )
		{			
			m_UIplayerInfo.SetLivesLeft(m_lives--);
		}
	}
}
