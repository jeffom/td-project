﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerManager : SingletonBehaviour<TowerManager>
{        
    /// <summary>
    /// Currently selected tower (for building or move if available)
    /// </summary>
    Tower m_pCurrentTower;

    /// <summary>
    /// Our grid reference (to find the exact snap position)
    /// </summary>
    [SerializeField]
    Grid m_pGrid;

    [SerializeField]
    Camera m_pInputCamera;

    [SerializeField]
    BuildMenu m_towerMenu;
    
    /// <summary>
    /// Data regarding available towers
    /// </summary>
    Dictionary<string, TowerData> m_dictData;

    public Tower SelectedTower
    {
        get
        {
            return m_pCurrentTower;
        }
        set
        {
            m_pCurrentTower = value;
        }
    }

    void Start()
    {
        InitTowerData();
        InputManager.OnClickEvent += InputManager_OnClickEvent;
    }

    void OnDestroy()
    {
        InputManager.OnClickEvent -= InputManager_OnClickEvent;
    }

    void InputManager_OnClickEvent(IClickable selectedObj)
    {
		if (IsBuildingTower())
		{
            CheckTowerBuilding();
        }
		else
		{
	        if (selectedObj != null && selectedObj.GetClickableType() == ClickableTypes.Tower)
	        {
	            SetSelection(selectedObj);
	        }
		}
    }

    void CheckTowerBuilding()
    {        
        //keep updating the position
        Node mousePosNode = MousePosToGrid();

        //no node to calculate things
        if (mousePosNode == null)
            return;

        bool isNodeAvailable = true;
        RaycastHit[] hits = Physics.SphereCastAll(
            mousePosNode.m_pPos,
            m_pGrid.nodeRadius,
            Vector3.forward,
            0,
            1 << LayerMask.NameToLayer("Clickable") | 1 << LayerMask.NameToLayer("Obstacle") | 1 << LayerMask.NameToLayer("Enemy")
        );

        if (hits != null)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                GameObject obj = hits[i].collider.gameObject;

                Debug.Log(obj.name);

                if (obj.GetComponentInParent<Tower>() != m_pCurrentTower) //checking if there's another object besides the constructing tower
                {
                    isNodeAvailable = false;
                    break;
                }

                if (obj.layer == LayerMask.NameToLayer("Enemy") || obj.layer == LayerMask.NameToLayer("Obstacle"))
                {
                    isNodeAvailable = false;
                    break;
                }
            }
        }

        if (isNodeAvailable)
        {
            mousePosNode.m_bIsWalkable = false;
            m_pCurrentTower.CurrentNode = mousePosNode;
            m_pCurrentTower.SnapPosition();
            m_pCurrentTower.SetFinishBuilding();
            UnsetSelection();

            NotifyTowerBuildFinish();
        }
    }


    void SetSelection(IClickable selectedObj)
    {
        if( m_pCurrentTower != null )
        {
            m_pCurrentTower.IsSelected = false;
        }

        Tower pTower = selectedObj.GetTransform().GetComponentInParent<Tower>();
        if(pTower != null )
        {
            pTower.IsSelected = true;
            
            m_pCurrentTower = pTower;
            if(m_towerMenu != null)
            {
                m_towerMenu.ShowTowerModifyMenu();
            }
        }
    }

    void UnsetSelection()
    {
        m_pCurrentTower = null;
    }

    void Update()
    {
		if( IsBuildingTower() )
		{
			//moving selected tower
			if( m_pCurrentTower.CanMove() )
			{
				//keep updating the position
				Node mousePosNode = MousePosToGrid();

				//check it if it's already occupied
				bool canSetPos = mousePosNode != null && mousePosNode != m_pCurrentTower.CurrentNode;

				if (canSetPos)
				{                                        
					m_pCurrentTower.CurrentNode = mousePosNode;
					m_pCurrentTower.SnapPosition();
				}                               
			}
		}
		else if( IsTowerSelected() )
        {
            
        }
    }

    Node MousePosToGrid()
    {
        if (m_pGrid != null)
        {
            Camera pCam = m_pInputCamera != null ? m_pInputCamera : Camera.main; ;

            Vector3 mouseScreenPos = Input.mousePosition;

            //converting screem to world pos
            Vector3 curMousPos = pCam.ScreenToWorldPoint(mouseScreenPos);

            return m_pGrid.NodeFromMouseInput(curMousPos);
        }
        else
        {
            Debug.LogError("Grid must be set");
            return null;
        }
    }

    void Log(string msg)
    {
        Debug.Log(msg);
    }

    #region Tower Datas

    void InitTowerData()
    {
        //hardcoded tower cofigs
        m_dictData = new Dictionary<string, TowerData>();

		m_dictData.Add(TowerType.Basic.ToString(), new TowerData(TowerType.Basic, 100, TowerType.Basic.ToString() ));
		m_dictData.Add(TowerType.Splash.ToString(), new TowerData(TowerType.Splash, 100, TowerType.Splash.ToString() ));
		m_dictData.Add(TowerType.Debug.ToString(), new TowerData(TowerType.Debug, 0, TowerType.Debug.ToString() ));
    }

    public TowerData GetTowerDataFromType(TowerType type)
    {
        if( m_dictData.ContainsKey(type.ToString()))
        {
            return m_dictData[type.ToString()];
        }

        return null;
    }

    #endregion 

	public void BuildTower(TowerType eType)
	{
		if(!IsBuildingTower())
		{
            if (CanBuildTower(eType))
            {
                //instantiate new tower
                string path = "Towers/" + eType.ToString();
                Tower prefab = Resources.Load<Tower>(path);
                Tower newTower = Instantiate(prefab) as Tower;
                newTower.transform.position = Vector3.one * 9999;
                //Node freeNode = m_pGrid.GetFreeNode();

                newTower.SetBuildingState();
                //newTower.CurrentNode = freeNode;
                newTower.transform.SetParent(transform);
                //newTower.SnapPosition();

                m_pCurrentTower = newTower;
            }
		}
	}

    bool CanBuildTower(TowerType eType)
    {
        return true;
    }

	bool IsTowerSelected()
	{
		return m_pCurrentTower != null;
	}

	bool IsBuildingTower()
	{
		return m_pCurrentTower != null && m_pCurrentTower.TowerState == TowerState.Building;
	}

	//
	void NotifyTowerBuildFinish()
	{
		WorldNotifierManager.TriggerEvent( WorldEventsType.TowerBuildingFinish.ToString() );
	}

    public void UpgradeSelectedTower()
    {
        if (m_pCurrentTower != null && m_pCurrentTower.CanBeUpgraded())
        {
            m_pCurrentTower.UpgradeTower();
        }
    }

    public void SellSelectedTower()
    {
        if( m_pCurrentTower != null )
        {
            Destroy(m_pCurrentTower.gameObject);
        }
    }
}
