﻿using UnityEngine;
using System.Collections;

//Interface for basic grid interaction
public interface IClickable
{
    bool IsSelected{ get; }
    bool CanMove();
    Node CurrentNode { get; set; }
    void SnapPosition();
    ClickableTypes GetClickableType();
    void OnClick();
    Transform GetTransform();
}
