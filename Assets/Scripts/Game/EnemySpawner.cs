﻿using UnityEngine;
using System.Collections;
using System;

public class EnemySpawner : MonoBehaviour
{

    EnemyType m_enemyType;
    int m_amount;
    Transform m_enemiesObjective;

    float m_timeBetweenSpawns = 0.5f;
    string m_enemiesResourcesPath = "Enemies/";

    Action<Enemy> m_delOnSpawn;

    public int SpawnsRemaining
    {
        get
        {
            return m_amount;
        }
    }

    public void StartSpawning(WaveConfig cfg, Transform objective, int iWave, Action<Enemy> onSpawn)
    {
        StartSpawning(
            cfg.type,
            cfg.amount,
            cfg.timeBetweenSpawns,
            objective,
            iWave,
            onSpawn
        );
    }

    public void StartSpawning(EnemyType type, int amount, float timebetweenSpawns, Transform objective, int iWave , Action<Enemy> onSpawn)
    {
        m_enemyType = type;
        m_amount = amount;
        m_enemiesObjective = objective;
        m_timeBetweenSpawns = timebetweenSpawns;
        StartCoroutine(SpawnEnemies(iWave));
        m_delOnSpawn = onSpawn;
    }

    IEnumerator SpawnEnemies(int iWave)
    {
        while (m_amount > 0)
        {
            m_amount--;            
            EnemyType type = m_enemyType;
            Enemy prefab = Resources.Load<Enemy>(m_enemiesResourcesPath + type.ToString());
            Enemy newEnemy = Instantiate(prefab, Vector3.one * 9999f, Quaternion.identity) as Enemy;

            yield return null;

            newEnemy.SetLevel(iWave);
            newEnemy.transform.position = transform.position;
            
            newEnemy.StartPathFinding(m_enemiesObjective);
            m_delOnSpawn(newEnemy);

            yield return new WaitForSeconds(m_timeBetweenSpawns);
        }
    }
}
