﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemiesManager : MonoBehaviour
{

    public List<Enemy> m_listEnemy;
    public float m_fTimeUntilNextWave = 5f;

    //string m_enemiesResourcesPath = "Enemies/";

    [SerializeField]
    WaveConfig[] m_waves;

    [SerializeField]
    EnemySpawnConfig[] m_pSpawnConfigs;

    [SerializeField]
    WaveInfo m_UIWaveInfo;

    int m_waveIndex = 0;

    void Awake()
    {
        WorldNotifierManager.notificationListener += WorldEventsHandler;
    }

    void OnDestroy()
    {
        WorldNotifierManager.notificationListener -= WorldEventsHandler;
    }

    void Start()
    {
        StartCoroutine(StartWaves());
    }

    public void LoadLevel(int i)
    {
        m_listEnemy.Clear();
    }

    IEnumerator StartWaves()
    {
        yield return null;

        while (m_waveIndex < m_waves.Length)
        {
            m_UIWaveInfo.SetWave(m_waveIndex);

            Debug.Log("starting wave:" + m_waveIndex);
            //create a new Spawner

            //for now just using the first index of the objectives
            EnemySpawnConfig cfg = m_pSpawnConfigs[0];

            EnemySpawner spwner = new GameObject().AddComponent<EnemySpawner>();
            spwner.name = "Enemy Spawner";
            spwner.transform.SetParent(transform);
            spwner.transform.position = cfg.m_spawnPoint.transform.position;

            yield return null;

            spwner.StartSpawning(
                m_waves[m_waveIndex],
                cfg.m_objective,
                m_waveIndex,
                OnSpawnEnemy
            );

            while (spwner.SpawnsRemaining > 0)
            {
                yield return null;
                m_UIWaveInfo.SetSpawnInfo(spwner.SpawnsRemaining);
            }

            yield return new WaitForSeconds(m_waves[m_waveIndex].nextWaveTime);

            //Destroy(spwner.gameObject);
            m_waveIndex++;
        }
    }

    void OnSpawnEnemy(Enemy pEnemy)
    {
        pEnemy.transform.SetParent(transform);
        m_listEnemy.Add(pEnemy);        
    }

    void RecalculateUnitsPath()
    {        
        for (int i = m_listEnemy.Count-1; i >= 0 ; i-- )
        {
            if( m_listEnemy[i].IsDead() || m_listEnemy[i] == null )
            {
                m_listEnemy.RemoveAt(i);
            }
            else
            {
                m_listEnemy[i].RecalculatePath();
            }
        }
    }
    
    void WorldEventsHandler(string evt, params object[] p)
    {
        try
        {
            //testing for grid changing events
            if (
                evt == WorldEventsType.TowerBuildingFinish.ToString() ||    
                evt == WorldEventsType.TowerDestroyed.ToString()
            )
            {
                //grid has changed we must recalculate the path
                RecalculateUnitsPath();
            }
            else if( evt == WorldEventsType.EnemyReachedGoal.ToString() )
            {
                if(p.Length > 0 && p[0] is Enemy)
                {
                    Enemy enemy = p[0] as Enemy;
                    if( m_listEnemy.Contains(enemy) )
                        m_listEnemy.Remove(enemy);
                    Destroy(enemy.gameObject);
                }
            }
        }
        catch
        {
            Debug.LogError("Exception, probably caused by missing reference");
        }
    }
}

/// <summary>
/// Enemy spawn config.
/// </summary>
[System.Serializable]
public struct EnemySpawnConfig
{
    public Transform m_spawnPoint;
    public Transform m_objective;
}

[System.Serializable]
public struct WaveConfig
{
    public int amount;
    public float nextWaveTime;
    public EnemyType type;
    [Range(0.5f,5f)]
    public float timeBetweenSpawns;
}