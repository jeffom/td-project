﻿using UnityEngine;
using System.Collections;

public class TowerData
{
    public string m_name;
    public TowerType m_towerType;
    public int m_buildCost;

    public TowerData(TowerType type, int cost, string name="")
    {
        m_towerType = type;
        m_buildCost = cost;
        if( string.IsNullOrEmpty(name) )
        {
			m_name = type.ToString();
        }
    }

}
