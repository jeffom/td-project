﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UnitLifeBar : MonoBehaviour {

	[Range(0,100)]
	public float m_lifePercentage;

	[SerializeField]
	RectTransform m_healthBarForeground;

	[SerializeField]
	RectTransform m_healthBarBackground;
    	
	public Transform m_target;

	public void SetHealth(float fValue)
	{
		fValue = Mathf.Clamp01(fValue);

		m_healthBarForeground.sizeDelta = new Vector2( fValue * m_healthBarBackground.rect.width , m_healthBarForeground.sizeDelta.y );
	}

	public void SetBarEnabled(bool enabled)
	{
		this.gameObject.SetActive(enabled);
	}

}
