﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerInfo : MonoBehaviour {

	[SerializeField]
	Text m_labelLives;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void SetLivesLeft(int iLives)
	{
		m_labelLives.text = "Lives:" + iLives;
	}
}
