﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TowerInfo : MonoBehaviour {

    [SerializeField]
    Text m_labelLevel;

    public void SetLevel(int iLevel)
    {
        m_labelLevel.text = iLevel.ToString();
    }
}
