﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaveInfo : MonoBehaviour {

	[SerializeField]
	Text m_labelWaveInfo;

	[SerializeField]
	Text m_labelSpawnsRemainingInfo;

	string m_waveText; 
	string m_spawnText;

	void Start()
	{
		m_waveText = m_labelWaveInfo.text;
		m_spawnText = m_labelSpawnsRemainingInfo.text;
	}

	public void SetWave(int i)
	{
		m_labelWaveInfo.text = m_waveText + i;
	}


	public void SetSpawnInfo(int i)
	{
		m_labelSpawnsRemainingInfo.text = m_spawnText + i;
	}
}
