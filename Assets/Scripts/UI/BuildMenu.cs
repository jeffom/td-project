﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// Ui for selecting what to build
/// </summary>
public class BuildMenu : MonoBehaviour
{

    [SerializeField]
    GameObject m_initialMenu;

    [SerializeField]
    GameObject m_towerSelectMenu;

    [SerializeField]
    GameObject m_towerModifyMenu;

    [SerializeField]
    GridLayoutGroup m_towerButtonsGrid;

    GameObject[] m_menuPanels;
    Dictionary<string, TowerData> m_dictData;

    // Use this for initialization
    void Start()
    {
        m_menuPanels = new GameObject[] {
            m_initialMenu, m_towerSelectMenu, m_towerModifyMenu            
        };

        ShowInitialMenu();

        //fetch data
        //associate with button
        foreach (TowerType e in System.Enum.GetValues(typeof(TowerType)))
        {
            TowerData data = TowerManager.Instance.GetTowerDataFromType(e);
            if (data != null)
            {
                //trying to instantiate a new button
                Button buttonPrefab = UnityEngine.Resources.Load<Button>("UI/Button");
                Button instance = (Button)UnityEngine.Object.Instantiate(buttonPrefab);

                Text uitext = instance.GetComponentInChildren<Text>();

				uitext.text = "Build " + data.m_towerType;
                uitext.text += "\nCost:" + data.m_buildCost ;
                        
                instance.transform.SetParent(m_towerButtonsGrid.transform);
				instance.transform.localScale = Vector3.one;
				instance.onClick.AddListener( delegate(){
					this.BuildTower(data.m_towerType.ToString());
				});
            }
        }
    }
    
    public void ShowInitialMenu()
    {
        HidePanels();
        m_initialMenu.SetActive(true);
    }

    public void ShowTowerBuildMenu()
    {
        HidePanels();
        m_towerSelectMenu.SetActive(true);

    }
    
    public void ShowTowerModifyMenu()
    {
        HidePanels();
        m_towerModifyMenu.SetActive(true);
    }

    void HidePanels()
    {
        for(int i=0; i < m_menuPanels.Length; i++)
        {
            m_menuPanels[i].SetActive(false);
        }
    }

    public void BuildTower(string type)
    {
        try
        {
            TowerType newTowertype = (TowerType)System.Enum.Parse(typeof(TowerType), type);
			TowerManager.Instance.BuildTower(newTowertype);
        }
        catch
        {
            Debug.LogError("Invalid tower type");
        }
    }

    public void UpgradeTower()
    {
        TowerManager.Instance.UpgradeSelectedTower();
    }

    public void SellTower()
    {
        TowerManager.Instance.SellSelectedTower();
        ShowTowerBuildMenu();
    }
}
