﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Simple Single Pattern for reusability
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{

    protected static T m_gInstance;

    public static T Instance
    {
        get
        {
            if (m_gInstance == null)
            {
                m_gInstance = FindObjectOfType<T>();
            }

            return m_gInstance;
        }
    }

    protected void Awake()
    {
        //m_gInstance = this;
        m_gInstance = this as T;
    }
}
