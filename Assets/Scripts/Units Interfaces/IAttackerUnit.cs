﻿using UnityEngine;
using System.Collections;

public interface IAttackerUnit {
    float AttackDamage { get; }
    void Attack();
    bool CanAttack();
}
