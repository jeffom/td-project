﻿using UnityEngine;
using System.Collections;

public interface ILifeUnit
{
    bool IsDead();
    float Life { get; }
    float Armor { get; }
    void TakeDamage(float iAmount);
    void Die(); //all that lives must die someday muwhaha    
}
