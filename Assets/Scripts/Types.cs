﻿using UnityEngine;
using System.Collections;

public enum ClickableTypes
{
    Tower = 0,
}

public enum TowerState
{
    Idle = 0,
    Building,   //selected some tower for building
    Upgrading,  //being upgraded
}

public enum TowerType
{
    Debug,
    Basic,
    Splash
}

public enum EnemyType
{
	Enemy_01,
	Enemy_02,
}

public enum WorldEventsType
{
	TowerBuildingFinish,
    TowerDestroyed,

	EnemyReachedGoal,
	GameOver,
}