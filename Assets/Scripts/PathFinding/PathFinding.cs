﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System;

public class PathFinding : MonoBehaviour {

    PathRequesterManager m_pRequestManager;
    Grid m_pGrid;
    
    void Awake()
    {
        m_pRequestManager = GetComponent<PathRequesterManager>();
        m_pGrid = GetComponent<Grid>();
    }


    /// <summary>
    /// Basic A* algorithm implementation
    /// </summary>
    /// <param name="startPos"></param>
    /// <param name="targetPos"></param>
    /// <returns></returns>
    IEnumerator FindPath(Vector3 startPos, Vector3 targetPos)
    {        
        Node startNode = m_pGrid.NodeFromWorldPoint(startPos);
        Node targetNode = m_pGrid.NodeFromWorldPoint(targetPos);
                
        startNode.m_gCost = GetDistance(startNode, targetNode);

        Node lowestCostNode = null;    //trying to keep track of the most reachable node before objective

        
        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();

        bool pathSuccess = false;
        Vector3[] wayPoints = null;

        openSet.Add(startNode);

        while( openSet.Count > 0 ){
                        
            Node currentNode = openSet[0];
            for(int i = 1; i < openSet.Count; i++)
            {
                if ( openSet[i].fCost < currentNode.fCost || 
                    (openSet[i].fCost == currentNode.fCost && openSet[i].m_hCost < currentNode.m_hCost)
                    )
                    currentNode = openSet[i];
            }

            openSet.Remove(currentNode);
            closedSet.Add(currentNode);

            if (currentNode == targetNode)
            {                       
                pathSuccess = true;
                break;
            }
            else
            {
                if (currentNode.m_hCost > 0 && (lowestCostNode == null || currentNode.CompareTo(lowestCostNode) > 0) )
                    lowestCostNode = currentNode;
            }

            foreach(Node neighbour in m_pGrid.GetNeighbours(currentNode) )
            {                
                Vector3 direction = (neighbour.m_pPos - currentNode.m_pPos).normalized;
                RaycastHit hitinfo;
                bool canWalkToNeightour = !Physics.SphereCast(currentNode.m_pPos, m_pGrid.nodeRadius, direction, out hitinfo, m_pGrid.nodeRadius * 2, m_pGrid.unwalkableMask.value);

                if (!neighbour.m_bIsWalkable || closedSet.Contains(neighbour) || !canWalkToNeightour)
                {
                    continue;
                }

                int newMovementCostToNeighbour = currentNode.m_gCost + GetDistance(currentNode, neighbour);
                if( newMovementCostToNeighbour < neighbour.m_gCost || !openSet.Contains(neighbour) )
                {
                    neighbour.m_gCost = newMovementCostToNeighbour;             //cost to move to it from current pos
                    neighbour.m_hCost = GetDistance(neighbour, targetNode);     //cost from distance
                    neighbour.m_pParent = currentNode;

                    if( !openSet.Contains(neighbour) )
                    {
                        openSet.Add(neighbour);
                    }
                }
            }
        }

        yield return null;

        if( pathSuccess )
        {
            wayPoints = RetracePath(startNode, targetNode);            
        }
        else
        {
            wayPoints = RetracePath(startNode, lowestCostNode);
        }

        m_pRequestManager.FinishedProcessingPath(wayPoints, pathSuccess);
    }

    /// <summary>
    /// The Backtrack function
    /// </summary>
    /// <param name="startNode"></param>
    /// <param name="endNode"></param>
    Vector3[] RetracePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();

        Node currentNode = endNode;

        while(currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.m_pParent;
        }


        Vector3[] waypoints = new Vector3[path.Count];
        for(int i=0 ; i < path.Count; i++)
        {
            waypoints[i] = path[i].m_pPos;
        }

        //Vector3[] waypoints = SimplifyPath(path);

        Array.Reverse(waypoints);
        //waypoints.Reverse();

        return waypoints;
    }

    /// <summary>
    /// Not checking agains collisions to the next node,
    /// TODO create that check if we have some spare time
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    Vector3[] SimplifyPath (List<Node> path)
    {
        List<Vector3> waypoints = new List<Vector3>();
        Vector2 directionOld = Vector2.zero;

        for(int i=1; i < path.Count; i++)
        {
            Vector2 directionNew = new Vector2(path[i - 1].m_gridX - path[i].m_gridX, path[i - 1].m_gridY - path[i].m_gridY);
            if( directionNew != directionOld )
            {
                waypoints.Add(path[i].m_pPos);
            }

            directionOld = directionNew;
        }

        return waypoints.ToArray();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="nodeA"></param>
    /// <param name="nodeB"></param>
    /// <returns></returns>
    int GetDistance(Node nodeA, Node nodeB)
    {
        //heuristic function
        int distX = Mathf.Abs(nodeA.m_gridX - nodeB.m_gridX);
        int distY = Mathf.Abs(nodeA.m_gridY - nodeB.m_gridY);

        //the h(x) function is chosen using size = 1 and diagonal = 1.4
        //scaling * 10 for simplicity

        //lowest number dictates how many diagonals
        if( distX > distY )
        {
            return 14 * distY + 10 * (distX - distY);
        }

        return 14 * distX + 10 * (distY - distX);

    }


    internal void StartFindPath(Vector3 startPos, Vector3 targetPos)
    {
        StartCoroutine(FindPath(startPos, targetPos));
    }
}
