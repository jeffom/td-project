﻿#define VISUAL_DEBUG

using UnityEngine;
using System.Collections;
using System;

public class Unit : MonoBehaviour {

    public bool seekPathOnStart;
    public Transform target;
    public float speed = 5;

    Vector3[] path;
    public int targetIndex;

	Action m_onObjectiveReach;
    Action m_onBlockageReached;    //execute an action after reaching the lowest cost path

	bool m_isRunning;
    bool m_isBlocked;

	void Start()
	{
        if (seekPathOnStart && target != null)
        {
            //FindPath(null);
        }
	}

    void OnDestroy()
    {
        StopAllCoroutines();
    }

	public void FindPath(Action delOnObjectiveReach=null, Action delOnBlockReached=null)
	{		
		m_onObjectiveReach = delOnObjectiveReach;
        m_onBlockageReached = delOnBlockReached;

        if( target != null )
		    PathRequesterManager.RequestPath(transform.position, target.position, OnPathFound);
	}

    void OnPathFound(Vector3[] newPath, bool successful)
    {
        m_isBlocked = !successful;

        //if (successful)
        {
            path = newPath;
            targetIndex = 0;

            if (path != null)
            {
                StopAllCoroutines();
                StartCoroutine(FollowPath());
            }
        }
    }
    

    IEnumerator FollowPath()
    {
        if( path == null || path.Length == 0)
        {
            FindPath(m_onObjectiveReach, m_onBlockageReached);
            yield break;
        }

		//Debug.Log("Starting Follow Path");
		Vector3 currentWaypoint = path[0];

        while(true)
        {
			if( Vector3.Distance(transform.position, currentWaypoint) < 0.1f )
            {
                targetIndex++;
                if( targetIndex >= path.Length )
				{
					targetIndex = 0;
					path = null;
                    					                                        
                    if (m_isBlocked)
                    {
                        if (m_onBlockageReached != null)
                            m_onBlockageReached();

                        m_onBlockageReached = null;
                    }
                    else
                    {
                        if (m_onObjectiveReach != null)
                            m_onObjectiveReach();
                        m_onObjectiveReach = null;
                    }                                                           

                    yield break;
                }

                currentWaypoint = path[targetIndex];
            }


			transform.position = Vector3.MoveTowards(transform.position, currentWaypoint, speed * Time.deltaTime);
            yield return null;
        }  

    }

#if VISUAL_DEBUG

    void OnDrawGizmos()
    {
        if (path != null)
        {
            for (int i = targetIndex; i < path.Length; i++)
            {
                Gizmos.color = Color.black;
                Gizmos.DrawCube(path[i], Vector3.one);

                if (i == targetIndex)
                {
                    bool canWalkToNeightour = Physics.Raycast(transform.position, path[i] - transform.position, 8,
                        1 << LayerMask.NameToLayer("Clickable") | 1 << LayerMask.NameToLayer("Obstacle"));

                    if (canWalkToNeightour)
                        Gizmos.color = Color.red;

                    Gizmos.DrawRay(transform.position, (path[i] - transform.position).normalized);

                    //Gizmos.DrawLine(transform.position, path[i]);
                }
            }
        }

        Gizmos.DrawWireSphere(transform.position, 2);
    }

#endif

}
