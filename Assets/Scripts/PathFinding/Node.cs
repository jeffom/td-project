﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Using Heap to optimize the 
/// find path calculation
/// </summary>
public class Node : IHeapItem<Node>
{

    public bool m_bIsWalkable;
    public Vector3 m_pPos;

    public int m_gridX;
    public int m_gridY;

    public int m_gCost;
    public int m_hCost;

    int m_heapIndex;

    public Node m_pParent;

    public Node(bool walkable, Vector3 worldPos, int _gridX, int _gridY)
    {
        m_bIsWalkable = walkable;
        m_pPos = worldPos;
        m_gridX = _gridX;
        m_gridY = _gridY;
    }


    public int fCost
    {
        get
        {
            return m_gCost + m_hCost;
        }
    }

    public int HeapIndex
    {
        get
        {
            return m_heapIndex;
        }
        set
        {
            m_heapIndex = value;
        }
    }

    public int CompareTo(Node nodeToCompare)
    {
        //we want to know the priority of the lower cost function
        int compare = fCost.CompareTo(nodeToCompare.fCost);
        if (compare == 0)
        {
            compare = m_hCost.CompareTo(nodeToCompare.m_hCost);
        }

        return -compare;
    }


    public void DumpInfo()
    {
        Debug.Log(
            "X:" + this.m_gridX +
            "\nY" + this.m_gridY +
            "\nHeap Index" + this.HeapIndex +
            "\nPos:" + m_pPos +
            "");
    }
}
