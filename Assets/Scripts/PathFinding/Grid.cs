﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour
{

    public bool displayGridGizmos;
    public LayerMask unwalkableMask;
    public Vector2 gridWorldSize;
    public float nodeRadius;
    Node[,] grid;

    float nodeDiameter;
    int gridSizeX, gridSizeY;

    public List<Node> path;

	public List<LineRenderer> gridLines;

	[SerializeField]
	LineRenderer lineToInstantiate;

    public int MaxSize
    {
        get
        {
            return gridSizeX * gridSizeY;
        }
    }

    void Awake()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }

    void CreateGrid()
    {
        grid = new Node[gridSizeX, gridSizeY];
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;
		Vector3 worldTopRight = transform.position + Vector3.right * gridWorldSize.x / 2 + Vector3.forward * gridWorldSize.y / 2;

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));
                
                grid[x, y] = new Node(walkable, worldPoint, x, y);

            }
        }


		if( lineToInstantiate != null )
		{
			for (int x = 0; x <= gridSizeX; x++)
			{
				var newLineX = Instantiate(lineToInstantiate);
				//var newLineY = Instantiate(lineToInstantiate);
				newLineX.transform.parent = transform;
				newLineX.SetColors(Color.gray, Color.gray);
				//newLineX.SetWidth(0.05f,0.05f);
				newLineX.SetVertexCount(2);
				newLineX.SetPositions(
					new Vector3[]{
						new Vector3(worldBottomLeft.x +  x * nodeDiameter, 0.1f, worldBottomLeft.z) , 
						new Vector3(worldBottomLeft.x + x * nodeDiameter, 0.1f, worldTopRight.z)
					}
				);
			}


			for (int y = 0; y <= gridSizeY; y++)
			{
				var newLineY = Instantiate(lineToInstantiate);
				newLineY.transform.parent = transform;
				newLineY.SetColors(Color.gray, Color.gray);
				//newLineX.SetWidth(0.05f,0.05f);
				newLineY.SetVertexCount(2);
				newLineY.SetPositions(
					new Vector3[]{
						new Vector3(worldBottomLeft.x, 0.1f, worldBottomLeft.z + y * nodeDiameter) , 
						new Vector3(worldTopRight.x, 0.1f, worldBottomLeft.z + y * nodeDiameter)
					}
				);
			}
		}

    }

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                {
                    //it's the starting node
                    continue;
                }

                int checkX = node.m_gridX + x;
                int checkY = node.m_gridY + y;

                //check if it's within the grid
                if (checkX >= 0 && checkX < gridSizeX &&
                    checkY >= 0 && checkY < gridSizeY
                    )
                {
                    neighbours.Add(grid[checkX, checkY]);
                }
            }
        }

        return neighbours;
    }

    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        float percentX = (worldPosition.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float percentY = (worldPosition.z + gridWorldSize.y / 2) / gridWorldSize.y;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);
        return grid[x, y];
    }

    public Node NodeFromMouseInput(Vector3 mouseWorldPos)
    {
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;
        Vector3 worldTopRight = transform.position + Vector3.right * gridWorldSize.x / 2 + Vector3.forward * gridWorldSize.y / 2;

        mouseWorldPos.x = Mathf.Clamp(mouseWorldPos.x, worldBottomLeft.x, worldTopRight.x);
        mouseWorldPos.z = Mathf.Clamp(mouseWorldPos.z, worldBottomLeft.z, worldTopRight.z);

        //int nodeX = (int) mouseWorldPos.x

        float percentX = (mouseWorldPos.x + gridWorldSize.x / 2) / gridWorldSize.x;
        float percentY = (mouseWorldPos.z + gridWorldSize.y / 2) / gridWorldSize.y;

        int x = (int)((gridSizeX) * percentX);
        int y = (int)((gridSizeY) * percentY);

        if (x >= gridSizeX || x < 0 || y >= gridSizeY || y < 0)
            return null;
        else
            return grid[x, y];
    }

	public Node GetFreeNode()
	{
		return grid[0, 0];

//		for (int x = 0; x < gridSizeX; x++)
//		{
//			for (int y = 0; y < gridSizeY; y++)
//			{
//			}
//		}
	}

//	void OnGUI()
//	{
//		Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;
//		Vector3 worldTopRight = transform.position + Vector3.right * gridWorldSize.x / 2 + Vector3.forward * gridWorldSize.y / 2;
//
//		for (int x = 0; x < gridSizeX; x++)
//		{
//			GUI.color = Color.black;
//
//			Debug.DrawLine( 
//				new Vector3(worldBottomLeft.x +  x * nodeDiameter, 5, worldBottomLeft.z) , 
//				new Vector3(worldBottomLeft.x + x * nodeDiameter, 5, worldTopRight.z) ,
//				Color.black
//			);
//
//		}
//
//
//		for (int y = 0; y < gridSizeY; y++)
//		{
//		}
//	}

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1, gridWorldSize.y));

        if (grid != null && displayGridGizmos)
        {
            foreach (Node n in grid)
            {
                Gizmos.color = n.m_bIsWalkable ? Color.white : Color.red;

                Gizmos.DrawWireCube(n.m_pPos, new Vector3(1, 0.2f, 1) * (nodeDiameter - .1f));
            }
        }        
    }

    /// <summary>
    /// Creating this function for debug and visualisation purposes only
    /// </summary>
    [ContextMenu("Create New Grid")]
    void EditorCreateGrid()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }

#endif

}

