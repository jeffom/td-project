﻿using UnityEngine;
using System.Collections;

public static class Consts  {
    public const float GRID_RADIUS = 2f;
    public const int MAX_TOWER_LEVEL = 10;
}
