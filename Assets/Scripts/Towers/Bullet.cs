﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float m_damage;
	public float m_speed;
	public float m_effectRadius;

	public Vector3 m_destination;	//for aoe stuff
	public Vector3 m_source;
	public Enemy m_enemyTarget;			

	bool m_isLaunching;

	public void Launch()
	{
		m_isLaunching = true;
		m_source = this.transform.position;
	}

	void Update()
	{
		if( m_isLaunching )
		{
			if( this.transform.position != m_destination )
			{
				this.transform.position = Vector3.MoveTowards(this.transform.position, m_destination, m_speed * Time.deltaTime );
			}
			else
			{
				Hit();
			}
		}
	}

	void Hit()
	{		
		if( m_enemyTarget != null )
		{
			m_enemyTarget.TakeDamage( m_damage );
		}
		else
		{
			m_isLaunching = false;

			if( m_effectRadius > 0 )
			{
				RaycastHit[] hits = Physics.SphereCastAll(
					transform.position,
					m_effectRadius,
					Vector3.forward,
					0,
					1 << LayerMask.NameToLayer("Enemy") 
				);

				for(int i=0 ; i < hits.Length; i++)
				{
					Enemy enemyInRange = hits[i].collider.transform.GetComponentInParent<Enemy>();
					if( enemyInRange != null )
					{
						enemyInRange.TakeDamage(m_damage);
					}
				}
			}
		}

		Destroy(this.gameObject); //for now self destroy, if we have time create a pool object for this guy
	}
}
