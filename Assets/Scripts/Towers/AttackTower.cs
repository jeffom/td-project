﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class AttackTower : Tower, IAttackerUnit
{
    public bool drawRange = false;
    //basic properties
    public float m_damage;
    public float m_fireRate; //Interval in Seconds for each attack limited by time between frames
    public float m_attackRadius; //tower range

    //shot bullet settings
    public float m_bulletSpeed = 0;
    public float m_bulletRadius = 0f;
    public string m_bulletResourcesPath = "Bullets/Basic_Bullet";

    protected Enemy m_currentTarget; //will be shot until it leaves the range

    private float m_eElapsedShotTime = 0;

    #region Implements IAttackerUnit

    public virtual float AttackDamage
    {
        get
        {
            return m_damage;
        }
    }

    public virtual bool CanAttack()
    {
        return (m_eElapsedShotTime < Time.time && m_currentState == global::TowerState.Idle);
    }

    public virtual void Attack()
    {
        m_eElapsedShotTime = Time.time + m_fireRate;
    }

    #endregion

    protected void Update()
    {
        if (CanAttack())
        {
            Attack();
        }
    }


    protected virtual void LaunchBullet(Vector3 pPos)
    {
        //create instance
        Bullet bulletPrefab = Resources.Load<Bullet>(m_bulletResourcesPath);
        Bullet newBullet = Instantiate(bulletPrefab);
        newBullet.transform.position = this.transform.position;
        newBullet.m_damage = this.m_damage;
        newBullet.m_destination = pPos;
        newBullet.m_effectRadius = m_bulletRadius;
        newBullet.m_enemyTarget = m_currentTarget;
        newBullet.m_speed = m_bulletSpeed;
        
        newBullet.Launch();
    }

    protected virtual Enemy[] GetTargetsInRange()
    {
        List<Enemy> newtargets = new List<Enemy>();

        RaycastHit[] hits = Physics.SphereCastAll(
            transform.position,
            m_attackRadius,
            Vector3.forward,
            0,
            1 << LayerMask.NameToLayer("Enemy")
        );

        for (int i = 0; i < hits.Length; i++)
        {
            Enemy enemyInRange = hits[i].collider.transform.GetComponentInParent<Enemy>();
            if (enemyInRange != null)
            {
                newtargets.Add(enemyInRange);
            }
        }

        return newtargets.ToArray();
    }

#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (drawRange)
        {
            Gizmos.DrawWireSphere(transform.position, m_attackRadius);
        }
    }
#endif

    public override void UpgradeTower()
    {
        base.UpgradeTower();

        m_damage += m_damage * m_levelFactor;        
    }

}
