﻿using UnityEngine;
using System.Collections;

public class DebugTower : Tower {

    void Start()
    {
        SetBuildingState();
    }

    public override bool CanMove()
    {
        return m_currentState == global::TowerState.Building ;
    }

	public override void SetFinishBuilding ()
	{
		base.SetFinishBuilding ();
		Material mat = GetComponentInChildren<MeshRenderer>().material;
		mat.color = Color.blue;
	}
    
}
