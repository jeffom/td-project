﻿using UnityEngine;
using System.Collections;

public class BasicTower : AttackTower
{   
    [SerializeField]
    Transform m_gunTransform;    

    #region Tower Overrides
    
    public override bool CanMove()
    {
        return m_currentState == TowerState.Building;
    }

    public override void Attack()
    {
        if (m_currentTarget == null)
        {
            Enemy[] enemiesInRange = GetTargetsInRange();

            if (enemiesInRange != null && enemiesInRange.Length > 0 && enemiesInRange[0] != null)
            {
                m_currentTarget = enemiesInRange[0];

            }
        }


        if (m_currentTarget != null)
        {
            if (IsEnemyInRange(m_currentTarget))
            {
                m_gunTransform.LookAt(m_currentTarget.transform);

                base.Attack();
                LaunchBullet(m_currentTarget.transform.position);
            }
            else
            {
                m_currentTarget = null;
            }
        }
    }

    bool IsEnemyInRange(Enemy enemy)
    {
        return Vector3.Distance(this.transform.position, enemy.transform.position) <= m_attackRadius;
    }

    #endregion

}
