﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Our basic tower behaviour for controlling the towers
/// </summary>
public abstract class Tower : MonoBehaviour, IClickable, ILifeUnit
{
    protected Node m_pCurrentTowerNode;
    protected TowerState m_currentState;

    public float m_life;
    public float m_armor;
    public int m_iLevel = 1;
    public float m_levelFactor = 0.15f;
    bool m_isSelected;

    [SerializeField]
    TowerInfo m_towerInfo;

    public TowerState TowerState{
        get
        {
            return m_currentState;
        }   
        set
        {
            m_currentState = value;
        }
    }

    protected void Start()
    {        
        if (m_towerInfo != null)
        {            
            m_towerInfo.SetLevel(m_iLevel);
        }
    }

    #region Implements ILifeUnit
    public bool IsDead()
    {
        return m_life <= 0;
    }

    public float Life
    {
        get
        {
            return m_life;
        }
    }

    public float Armor
    {
        get
        {
            return m_armor;
        }
    }

    public void Die()
    {
        this.CurrentNode.m_bIsWalkable = true;  //the node is free again
        WorldNotifierManager.TriggerEvent(WorldEventsType.TowerDestroyed.ToString());          
        Destroy(gameObject);
    }

    public void TakeDamage(float fAmount)
    {
        //no point in taking damage when dead i guess
        if (IsDead())
            return;

        m_life -= fAmount;
        if (m_life <= 0)
            Die();
    }
    
    #endregion

    #region Implements IClickable

    public bool IsSelected
    {
        get
        {
            return m_isSelected;
        }
        set
        {
            m_isSelected = value;
            
            try
            {
                Material mat = GetComponentInChildren<MeshRenderer>().material;
                if (m_isSelected)
                {
                    mat.color = Color.green;
                }
                else
                {
                    mat.color = Color.grey;
                }
            }
            catch
            {
                Debug.Log("Couldn't get material");
            }
            
        }
    }

    public ClickableTypes GetClickableType()
    {
        return ClickableTypes.Tower;
    }

    public virtual bool CanMove()
    {
        return false;
    }

    public Node CurrentNode
    {
        get;
        set;
    }

    public void SnapPosition()
    {
        //snapping the tower position to a grid position
        if (CurrentNode != null)
        {
            transform.position = CurrentNode.m_pPos;
        }
    }

    public void OnClick()
    {

    }

    public Transform GetTransform()
    {
        return transform;
    }

    #endregion

    #region Tower Methods

    public virtual void LookAt(Transform pTarget)
    {
        Debug.LogError("Implement the LookAt method");        
    }

    #endregion

    #region Manager Methods

    public virtual void SetBuildingState()
    {
        gameObject.SetLayerRecursively(LayerMask.NameToLayer("Default"));
        m_currentState = TowerState.Building;
        try
        {
            Material mat = GetComponentInChildren<MeshRenderer>().material;
            mat.color = Color.red;
        }
        catch
        {
            Debug.Log("Couldn't get material");
        }
    }


    public virtual void SetFinishBuilding()
    {
        m_iLevel = 1;

        m_currentState = TowerState.Idle;
        gameObject.SetLayerRecursively(LayerMask.NameToLayer("Clickable"));

        try
        {
            Material mat = GetComponentInChildren<MeshRenderer>().material;
            mat.color = Color.grey;
        }
        catch
        {
            Debug.Log("Couldn't get material");
        }

    }

	public virtual  void StartUpgrading()
    {
		Debug.LogError("Implement the StartUpgrading method");
    }


    public virtual IEnumerator StartUpgrade()
    {
        yield return null;
    }


    public bool CanBePlaced()
    {
        return false;
    }

    #endregion 
    
    public virtual void UpgradeTower()
    {
        m_iLevel++;
        if( m_towerInfo != null )
        {
            m_towerInfo.SetLevel(m_iLevel);
        }
    }
    
    public virtual bool CanBeUpgraded()
    {
        return (m_iLevel < Consts.MAX_TOWER_LEVEL);        
    }        
}
