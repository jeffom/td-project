﻿#define DEBUG_LOG

using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Basic Enemy Behaviour
/// Move from A to B using the impelemented path find
/// </summary>
public class Enemy : MonoBehaviour, ILifeUnit, IAttackerUnit
{	
	public EnemyType m_enemyType;
	public Transform m_currentObjective;
    public Transform m_tTarget;

    protected NavMeshAgent m_pAgent;

	public float m_life;
	public float m_attackDamage;
	public float m_armor; 
    public float bounty_amount;
    public float m_attackRate = 1f;
    private float m_elapsedAttackTime = 0;

    public float m_levelfactor = 0.2f;
	protected float m_maxLife;

	[SerializeField]
	UnitLifeBar m_lifeBar;

	Unit m_unit;
    ILifeUnit m_currentMarkedTarget;


    public bool m_findPathOnStart = false;

    #region Implements ILifeUnit
    public float Armor
    {
        get
        {
            return m_armor;
        }
    }

    public float Life {
        get
        {
            return m_life;
        }
    }
    
    public void TakeDamage(float damage)
    {
        m_life -= Mathf.Clamp(damage - m_armor, 0.1f, 9999f);
        m_lifeBar.SetHealth(m_life / m_maxLife);

        if (m_life <= 0)
        {
            Die();
        }
    }
    
    public virtual void Die()
    {
        Destroy(this.gameObject);
    }

    #endregion

    #region Implements IAttackerUnit
    public bool IsDead()
    {
        return m_life <= 0;
    }

    public float AttackDamage
    {
        get
        {
            return m_attackDamage;
        }
    }

    public virtual void Attack()
    {
        m_elapsedAttackTime = Time.time + m_attackRate;

        if (m_currentMarkedTarget != null)
        {
            if (!m_currentMarkedTarget.IsDead())
            {
                m_currentMarkedTarget.TakeDamage(m_attackDamage);
            }
            else
            {
                m_currentMarkedTarget = null;
            }
        }
    }

    public virtual bool CanAttack()
    {
        return m_elapsedAttackTime < Time.time;
    }

    #endregion

    void Awake()
	{
		m_unit = GetComponent<Unit>();        
	}

    void Start()
    {
        m_lifeBar.SetBarEnabled(true);
        m_maxLife = m_life;
        m_lifeBar.SetHealth(m_life / m_maxLife);

        if( m_findPathOnStart && m_currentObjective != null )
        {
            RecalculatePath();
        }
    }

	void OnDestroy()
	{
        
	}

    void Update()
    {
        if(m_currentMarkedTarget != null )
        {
            if (CanAttack())
                Attack();
        }
    }

	void OnObjectiveReached()
	{        
		WorldNotifierManager.TriggerEvent(WorldEventsType.EnemyReachedGoal.ToString(), this);                
	}

    private void OnBlockedPathReached()
    {
        //target direction
        Vector3 dir = (m_currentObjective.position - transform.position).normalized;

        RaycastHit hitInfo;
        //search for attackable targets
        if ( Physics.SphereCast( transform.position, Consts.GRID_RADIUS, dir, out hitInfo, Consts.GRID_RADIUS * 2, 1 << LayerMask.NameToLayer("Clickable") ) )
        {
            ILifeUnit pUnit = hitInfo.transform.GetComponentInParent<ILifeUnit>();
            m_currentMarkedTarget = pUnit;
        }
    }


    public void StartPathFinding(Transform objective)
	{
		m_currentObjective = objective;
		m_unit.target = m_currentObjective;
		m_unit.FindPath(OnObjectiveReached, OnBlockedPathReached);
	}

    public void RecalculatePath()
	{        
        try
        {
            if( m_unit == null )
                m_unit = GetComponent<Unit>();

            m_unit.target = m_currentObjective;
            m_unit.FindPath(OnObjectiveReached, OnBlockedPathReached);
        }
        catch
        {
            Debug.LogError("Error while trying to recalculate path");
        }
	}
            
    public void SetLevel(int iLevel)
    {
        m_maxLife += m_maxLife * ( m_levelfactor * (iLevel+1) );
        m_life = m_maxLife;
        m_armor += m_armor * ( m_levelfactor * (iLevel + 1) );
    }        
}   
